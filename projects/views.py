from typing import Any
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
from projects.models import Project
from django.views.generic import ListView
# Create your views here.


@login_required
def project_list(request):
    project_list = Project.objects.filter(
        owner=request.user
    )  # name of all recipe.
    # recipe is a big list of every single recipe

    context = {
        "project_list": project_list,
    }
    return render(
        request, "projects/list.html", context
    )  # render says which...
    # html file to give it to


# context gives the data it pulls from the database to the html


@login_required
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    context = {"show_project": show_project}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)

class ProjectSearchView(ListView):
    model = Project
    template_name = 'projects/list.html'
    context_object_name = 'post'

    def get_queryset(self):
        query = self.request.GET.get('q')
        return Project.objects.filter(name__icontains=query).order_by("-name")
